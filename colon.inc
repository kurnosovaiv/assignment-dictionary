%define Next_w 0
%macro colon 2
	%ifid %2			
		%2: dq Next_w
		%define Next_w %2
	%else
		%fatal "ID expected"
	%endif
	%ifstr %1
		db %1, 0
	%else
		%fatal "Expected key value"
	%endif
%endmacro 
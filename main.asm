%include "words.inc"
%include "lib.inc"

%define N_SIZE 8
%define B_SIZE 255

extern find_word

global _start

section .rodata

k_nfound:
	db "this key not found", 0
buf_overflow:
	db "overflow of buffer", 0

section .bss
s_buf: resb B_SIZE

section .text

_start:
	xor rax, rax
	mov rdi, s_buf
	mov rsi, B_SIZE
	call read_word
	test rax, rax
	jne .buf_ok
	mov rdi, buf_overflow
	call print_err
	call print_newline_err
	mov rdi, 1
	call exit

.buf_ok:
	mov rdi, s_buf
	mov rsi, Next_w
	call find_word
	test rax, rax
	jne .key_ok
	jmp .key_err

.key_ok:				
	add rax, N_SIZE
	mov rdi, rax
	call print_string
	call print_newline
	mov rdi, 0
	call exit

.key_err:				
	mov rdi, k_nfound
	call print_err
	call print_newline_err
	mov rdi, 2
	call exit
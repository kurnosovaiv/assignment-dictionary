global exit
global string_length
global print_string
global print_err
global print_char
global print_newline
global print_newline_err
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

section .text

; ��������� ��� �������� � ��������� ������� �������
exit: 
	mov rax, 60     ; ��������� ����� 'exit'
	syscall

; ��������� ��������� �� ����-��������������� ������, ���������� � �����
string_length:
	xor rax, rax
.loop:
	cmp byte [rdi+rax], 0
	je .end
	inc rax
	jmp .loop
.end:
	ret

; ��������� ��������� �� ����-��������������� ������, ������� � � stdout

_print_string:
    push rsi
    push rax
    push rdx
    mov rsi, rdi
    call string_length
    mov rdx, rax
    mov rax, 1
    mov rdi, r10
    syscall
    pop rsi
    pop rax
    pop rdx
    ret

print_string:
    mov r10, 1
    call _print_string
    ret

print_err:
    mov r10, 2
    call _print_string
    ret


; ��������� ��� ������� � ������� ��� � stdout
print_char:
	push rdi
	mov rsi, rsp
	mov rdi, 1
	mov rdx, 1
	mov rax, 1
	syscall
	pop rdi
	ret

; ��������� ������ (������� ������ � ����� 0xA)

_print_newline:
	mov rdi, 0xA
	call print_char
	ret

print_newline:
	mov r10, 1
	call _print_newline
	ret

print_newline_err:
	 mov rdi, 2
	 call _print_newline
	 ret

; ������� ����������� 8-�������� ����� � ���������� ������� 
; �����: �������� ����� � ����� � ������� ��� ���������� �������
; �� �������� ��������� ����� � �� ASCII ����.
print_uint:
	mov rax, rdi
	mov r8, rsp
	mov rdi, 10
	push 0
	
.loop:
	xor rdx, rdx
	div rdi
	add rdx, '0'
	dec rsp
	mov [rsp], dl
	test rax, rax
	jnz .loop

	mov rdi, rsp
	call print_string
	add rsp, r8
	ret

; ������� �������� 8-�������� ����� � ���������� ������� 
print_int:
	test rdi, rdi
	jns .uns
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	neg rdi

.uns:
	call print_uint    
	ret

; ��������� ��� ��������� �� ����-��������������� ������, ���������� 1 ���� ��� �����, 0 �����
string_equals:
.loop:
	mov al, byte [rsi]
	cmp al, byte [rdi]
	jne .fail
	cmp al, 0
	je .save
	inc rsi
	inc rdi
	jmp .loop
.fail:
	xor rax, rax
	ret
.save:
	xor rax, rax
	inc rax
	ret


; ������ ���� ������ �� stdin � ���������� ���. ���������� 0 ���� ��������� ����� ������
read_char:
	xor rax, rax
	mov rdi, 0
	mov rdx, 1
	push 0
	mov rsi, rsp
	syscall
	pop rax
	ret

; ���������: ����� ������ ������, ������ ������
; ������ � ����� ����� �� stdin, ��������� ���������� ������� � ������, .
; ���������� ������� ��� ������ 0x20, ��������� 0x9 � ������� ������ 0xA.
; ��������������� � ���������� 0 ���� ����� ������� ������� ��� ������
; ��� ������ ���������� ����� ������ � rax, ����� ����� � rdx.
; ��� ������� ���������� 0 � rax
; ��� ������� ������ ���������� � ����� ����-����������

read_word:
	push r12
	push r13
	push r14
	mov r12, rdi
	mov r13, rsi
	dec r13
	xor r14, r14

.loop:
	cmp r14, r13
	jg .fail
	call read_char
	cmp al, 0xA
	je .suc
	cmp al, 0x9
	je .suc
	cmp al, 0x20
	je .suc
	cmp al, byte 0
	je .end
	mov byte[r12+r14], al
	inc r14
	jmp .loop
    
.fail:
	xor rax, rax
	pop r14
	pop r13
	pop r12
	ret
    
.suc:
	cmp r14, 0
	je .loop
    
.end:
	mov [r12+r14], al
	mov rax, r12
	mov rdx, r14
	pop r14
	pop r13
	pop r12
	ret

 

; ��������� ��������� �� ������, ��������
; ��������� �� � ������ ����������� �����.
; ���������� � rax: �����, rdx : ��� ����� � ��������
; rdx = 0 ���� ����� ��������� �� �������

parse_uint:
	xor rax, rax
	xor rcx, rcx
	xor rsi, rsi
	mov r10, 10
.loop:
	mov sil, [rdi+rcx]
	sub rsi, '0'
	js .end
	cmp rsi, 9
	jg .end
	mul r10
	add rax, rsi
	inc rcx
	jmp .loop
.end:
	mov rdx, rcx
	ret




; ��������� ��������� �� ������, ��������
; ��������� �� � ������ �������� �����.
; ���� ���� ����, ������� ����� ��� � ������ �� ���������.
; ���������� � rax: �����, rdx : ��� ����� � �������� (������� ����, ���� �� ���) 
; rdx = 0 ���� ����� ��������� �� �������

parse_int:
	xor rax, rax
	cmp byte [rdi], 0x2d
	je .parse
	call parse_uint
	ret
.parse:
	inc rdi
	call parse_uint
	cmp rdx, 0
	je .end
	neg rax
	inc rdx
.end:
	ret

; ��������� ��������� �� ������, ��������� �� ����� � ����� ������
; �������� ������ � �����
; ���������� ����� ������ ���� ��� ��������� � �����, ����� 0

string_copy:
	xor rax, rax
	xor rcx, rcx
	xor r9, r9
	push rdi
	call string_length
	pop rdi
	cmp rax, rdx
	jle .loop
	mov rax, 0
	ret
.loop:
	mov r9b, [rdi + rcx]
	mov [rsi + rcx], r9b
	inc rcx
	cmp byte[rsi + rcx], 0
	jne .loop
	mov rax, rdx
	ret


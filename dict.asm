global find_word
extern string_equals

section .text

find_word:
	.loop:
		test rsi, rsi
		jz .fail
		push rsi
		add rsi, 8
		call string_equals
		pop rsi
		test rax, rax
		jnz .suc
		mov rsi, [rsi]
		jmp .loop
	.fail:
		mov rax, 0
		ret
	.suc:
		mov rax, rsi
		ret